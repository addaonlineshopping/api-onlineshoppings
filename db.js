const sql = require("mssql");
const dotenv = require("dotenv");
dotenv.config();

const dbConfig = {
  user: "Sa",
  password: "Sa2008",
  server: "10.32.0.14",
  database: "DBonlinewholesale",
  stream: false,
  options: {
    enableArithAbort: true,
    encrypt: false
  },
  port: 1433,
};


const conn = new sql.ConnectionPool(dbConfig);
conn.connect((err) => {
  if (err) {
    console.log(err);
  }
});

const executeSQL = async (query) =>
  new Promise(async (resolve) => {
    try {
      const request = new sql.Request(conn);
      const result = await request.query(query);

      if (result.recordset !== undefined) {
        resolve(result.recordset);
      } else {
        resolve({
          status: "success",
          message: result.rowsAffected + " row affected",
        });
      }
    } catch (err) {
      //  console.error("SQL error", err);

      resolve({ status: "failed", message: err.message, sql: query });
    }
  });

const executeTransaction = (query) =>
  new Promise(async (resolve) => {
    const transaction = new sql.Transaction(conn);

    transaction.begin(async (err) => {
      try {
        const request = new sql.Request(transaction);
        for (let i = 0; i < query.length; i++) {
          await request.query(query[i]);
        }

        transaction.commit((err) => {
          if (err) {
            resolve({ status: "failed", message: "transaction commit error" });
          } else {
            resolve({
              status: "success",
              message: "transaction commit success",
            });
          }
        });
      } catch (error) {
        console.log(error);

        transaction.rollback((err) => {
          if (err) {
            resolve({ status: "failed", message: error.message });
          } else {
            resolve({ status: "failed", message: error.message, sql: query });
          }
        });
      }
    });
  });

module.exports.executeSQL = executeSQL;
module.exports.executeTransaction = executeTransaction;
