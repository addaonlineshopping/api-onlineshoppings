const crypto = require("crypto");
const multer = require("multer");
const genRandomString = (length) => {
  return crypto
    .randomBytes(Math.ceil(length / 2))
    .toString("hex") /** convert to hexadecimal format */
    .slice(0, length); /** return required number of characters */
};

const sha512 = (password, salt) => {
  let hash = crypto.createHmac("sha512", salt); /** Hashing algorithm sha512 */
  hash.update(password);
  let value = hash.digest("hex");
  return {
    salt: salt,
    passwordHash: value,
  };
};

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // cb(null, "D:/img-test/");
    cb(null, "../../../../home/share_windows/server14online/doc");
  },
  filename: function (req, file, cb) {
      cb(null,file.originalname);
  },
});

const upload = multer({ storage: storage }).array("file");

module.exports = {
  upload: upload,
  genRandomString: genRandomString,
  sha512: sha512,
};
