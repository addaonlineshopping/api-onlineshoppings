const express = require("express");
const cors = require("cors");
const app = express();
const bodyParser = require("body-parser");
const http = require("http").Server(app);

const url = "/api";

http.listen(7015, () => console.log("Server is running.."));
// http.listen(7012)

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(url + "/register", require("./routes/register"));
app.use(url + "/login", require("./routes/login"));
app.use(url + "/products",require("./routes/products"))

app.get("/", (req, res) => {
  res.sendStatus(404);
});
