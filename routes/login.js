const express = require("express");
const db = require("../db");
const moment = require("moment");
const router = express.Router();
const { sha512, genRandomString } = require("../funtion");

router.post("/", async (req, res) => {
  const { value } = req.body;
  let strSql = null;
  let result = null;

  if (value) {
    strSql = `SELECT customerID FROM customers WHERE email = '${value.email}'`;
    result = await db.executeSQL(strSql);
    if (result[0]) {
      const cusId = result[0].customerID;
      strSql = `SELECT pass,salt FROM users WHERE customerID = '${cusId}'`;
      result = await db.executeSQL(strSql);

      const passInput = sha512(value.password, result[0].salt).passwordHash;
      if (passInput === result[0].pass) {
        strSql = `SELECT customerID,allowed FROM customers WHERE customerID = '${cusId}'`;
        result = await db.executeSQL(strSql);
        if (result[0].allowed) {
          const token = genRandomString(20);
          strSql = `UPDATE users SET 
                    lastlogin = '${moment().format("YYYY-MM-DD HH:mm")}',
                    isexist='1',
                    token='${token}'
                    WHERE customerID='${cusId}'`;
          result = await db.executeSQL(strSql);
          if (result.status === "success") {
            res.send({
              success: true,
              status: true,
              allowed: true,
              token: value.username + ":" + token,
            });
          } else {
            res.send({
              success: true,
              status: false,
              result: result,
            });
          }
        } else {
          res.send({
            success: true,
            status: true,
            allowed: false,
          });
        }
      } else {
        res.send({
          success: true,
          status: false,
          message: "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง",
        });
      }
    } else {
      res.send({
        success: true,
        status: false,
        message: "ไม่พบผู้ใช้งาน",
      });
    }
  }
});

router.post("/getdoc", async (req, res) => {
  const { email } = req.body;
  console.log(email)
  if (!email) return;
  let strSql = `SELECT customerID FROM customers WHERE email ='${email}'`;
  let result = await db.executeSQL(strSql);
  if (result.status === "failed")
    return res.send({
      success: true,
      status: false,
      message: result,
    });
  const id = result[0].customerID;
  strSql = `SELECT * FROM reference_doc WHERE customerID = '${id}'`;
  result = await db.executeSQL(strSql);
  if (result.status === "failed")
    return res.send({
      success: true,
      status: false,
      message: result,
    });
  res.send({
    success: true,
    status: true,
    data: result,
  });
});

module.exports = router;
