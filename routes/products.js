const express = require("express");
const db = require("../db");

const router = express.Router();

router.get("/", async (req, res) => {
  const strSql1 = `SELECT DISTINCT pp.[type] ,t.[name] FROM product_prop AS pp INNER JOIN [type] AS t on pp.[type] = t.[type]`;
  const result1 = await db.executeSQL(strSql1);
  if (result1.status === "failed") {
    return res.send({ success: true, status: false, msg: result1 });
  }
  const strSql2 = `SELECT
                      pp.prodcode,
                      pp.[type],
                      t.[name],
                      pp.title,
                      pp.price,
                      pp.p_novat,
                      pp.thumbnail
                      FROM product_prop AS pp
                      INNER JOIN [type] AS t ON pp.[type] = t.[type]`;
  const result2 = await db.executeSQL(strSql2);
  if (result2.status === "failed") {
    return res.send({ success: true, status: false, msg: result5 });
  }

  res.send({ type: result1, items: result2 });
});

router.post("/detail", async (req, res) => {
  const { code } = req.body;

  let strSql = `SELECT * FROM product_prop WHERE prodcode = '${code}' ORDER BY prodcode`;
  const prodcode = await db.executeSQL(strSql);
  strSql = `SELECT 
            prodcode,
            pf.colorcode,
            image1,
            slide_img1,
            slide_img2,
            slide_img3,
            slide_img4,
            slide_img5,
            [desc] as color_name 
            FROM product_info pf  
            INNER JOIN acolor c ON c.colorcode = pf.colorcode
            WHERE pf.prodcode = '${code}'`;
  const colors = await db.executeSQL(strSql);
  strSql = `SELECT 
          p.prodcode,
          p.colorcode,
          s.packcode,
          pk.size01n,
          pk.size02n,
          pk.size03n,
          pk.size04n,
          pk.size05n,
          pk.size06n,
          pk.size07n,
          pk.size08n,
          pk.size09n,
          pk.size10n,
          pk.size11n,
          pk.size12n,
          pk.size01q,
          pk.size02q,
          pk.size03q,
          pk.size04q,
          pk.size05q,
          pk.size06q,
          pk.size07q,
          pk.size08q,
          pk.size09q,
          pk.size10q,
          pk.size11q,
          pk.size12q
          FROM product_info AS p 
          inner join shoed AS s ON p.prodcode = s.prodcode
          inner join packsize AS pk ON s.packcode = pk.packcode
          WHERE p.prodcode = '${code}'`;
  const colorSize = await db.executeSQL(strSql);
  
  if (prodcode.length !== 0) {
    const products = [];
    const colorsss = [];
    for (i = 0; i < prodcode.length; i++) {
      const c = await colors.filter((value) => {
        return value.prodcode === prodcode[i].prodcode;
      });
      for (j = 0; j < c.length; j++) {
        const s = await colorSize.filter((value) => {
          return (
            value.prodcode === c[j].prodcode &&
            value.colorcode === c[j].colorcode
          );
        });
        const re = s.map((item) => {
          return {
            packcode: item.packcode,
            size01n : item.size01n,
            size02n : item.size02n,
            size03n : item.size03n,
            size04n : item.size04n,
            size05n : item.size05n,
            size06n : item.size06n,
            size07n : item.size07n,
            size08n : item.size08n,
            size09n : item.size09n,
            size10n : item.size10n,
            size11n : item.size11n,
            size12n : item.size12n,
            size01q : item.size01q,
            size02q : item.size02q,
            size03q : item.size03q,
            size04q : item.size04q,
            size05q : item.size05q,
            size06q : item.size06q,
            size07q : item.size07q,
            size08q : item.size08q,
            size09q : item.size09q,
            size10q : item.size10q,
            size11q : item.size11q,
            size12q : item.size12q
          };
        });

        colorsss.push({
          colorcode: c[j].colorcode,
          color_name: c[j].color_name,
          image: c[j].image1,
          slide_img1: c[j].slide_img1,
          slide_img2: c[j].slide_img2,
          slide_img3: c[j].slide_img3,
          slide_img4: c[j].slide_img4,
          slide_img5: c[j].slide_img5,
          sizes: re,
        });
      }
      products.push({
        prodcode: prodcode[i].prodcode,
        title: prodcode[i].title,
        property: prodcode[i].property,
        description: prodcode[i].description,
        p_novat: prodcode[i].p_novat,
        colors: colorsss,
      });
    }
    res.send({ success: true, status: true, data: products });
  }
});

module.exports = router;
