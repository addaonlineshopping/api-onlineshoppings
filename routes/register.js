const express = require("express");
const moment = require("moment");
const numeral = require("numeral");
const db = require("../db");
const { genRandomString, sha512, upload } = require("../funtion");

const router = express.Router();

const getID = async () => {
  const date = moment().format("YY/MM").split("/");

  const strSql1 = `select max(customerID) as id from customers`;
  const result1 = await db.executeSQL(strSql1);

  const strSql2 = `select max(docno) as id from reference_doc`;
  const result2 = await db.executeSQL(strSql2);

  if (result1[0] !== undefined && result2[0] !== undefined) {
    if (result1[0].id !== null && result2[0].id !== null) {
      let dataID = {};
      if (date[0] + date[1] === result2[0].id.substring(1, 5)) {
        const c = parseInt(result1[0].id.substring(2));
        const r = parseInt(result2[0].id.substring(5));
        dataID = {
          customerID: "C2" + numeral(c + 1).format("00000"),
          ReferenceID: "R" + date[0] + date[1] + numeral(r + 1).format("0000"),
        };
      } else {
        const c = parseInt(result1[0].id.substring(2));
        const r = 0;
        dataID = {
          customerID: "C2" + numeral(c + 1).format("00000"),
          ReferenceID: "R" + date[0] + date[1] + numeral(r + 1).format("0000"),
        };
      }
      return dataID;
    } else {
      const c = 0;
      const r = 0;
      dataID = {
        customerID: "C2" + numeral(c + 1).format("00000"),
        ReferenceID: "R" + date[0] + date[1] + numeral(r + 1).format("0000"),
      };
      return dataID;
    }
  }
};

const insertCus = async (value, id) => {
  const strSql = `INSERT INTO customers
  (customerID,prefix,name,lastname,phone,email,idcard,birthday,company,address1,tumbol,amphur,province,country,zipcode,predate,allowed)
  VALUES
  ('${id.customerID}','${value.prefix}','${value.firstname}',
  '${value.lastname}','${value.phone.replace(/\s/g, "")}','${value.email}',
  '${value.idcard.replace(/\s/g, "")}','${moment(value.date).format(
    "YYYY/MM/DD"
  )}',
  '${value.company}','${value.address}','${value.amphur}','${value.tambol}',
  '${value.province}','TH','${value.zipcode}','${moment().format(
    "YYYY/MM/DD"
  )}',0)`;

  return strSql;
};

const insertRef = async (value, id) => {
  const strSql = `INSERT INTO reference_doc (docno,customerID) VALUES ('${id.ReferenceID}','${id.customerID}')`;
  return strSql;
};

const insertUser = async (value, id) => {
  const salt = genRandomString(30);

  const strSql = `INSERT INTO users (customerID,act_user,pass,salt,create_date) 
                  VALUES 
                  ('${id.customerID}','U',
                  '${sha512(value.password, salt).passwordHash}',
                  '${salt}','${moment().format("YYYY/MM/DD")}')`;
  return strSql;
};

router.get("/", async (req, res) => {
  let strSql = "";
  let result = "";
  const { idPro, idAmp } = req.query;
  if (idPro !== undefined) {
    strSql = `SELECT * FROM amphures WHERE province_id = ${idPro}`;
    result = await db.executeSQL(strSql);
    const dataAmphures = result;
    res.send({ data: dataAmphures });
  } else if (idAmp !== undefined) {
    strSql = `SELECT * FROM districts WHERE amphure_id  = ${idAmp}`;
    result = await db.executeSQL(strSql);
    const dataDistricts = result;
    res.send({ data: dataDistricts });
  } else {
    strSql = "SELECT * FROM provinces";
    result = await db.executeSQL(strSql);
    const dataProvinces = result;
    res.send(dataProvinces);
  }
});

router.post("/insertCustomers", async (req, res) => {
  const { value } = req.body;
  let arrayInsert = [];
  const id = await getID();
  if (id !== undefined) {
    const strChk = `SELECT email FROM customers WHERE email ='${value.email}'`;
    const result = await db.executeSQL(strChk);
    if (result[0]) {
      res.send({
        success: true,
        status: false,
        data: value.email,
      });
    } else {
      const strSqlCus = await insertCus(value, id);
      arrayInsert.push(strSqlCus);

      const strSqlUser = await insertUser(value, id);
      arrayInsert.push(strSqlUser);

      const strSqlRef = await insertRef(value, id);
      arrayInsert.push(strSqlRef);

      const result = await db.executeTransaction(arrayInsert);

      if (result.status === "success") {
        res.send({
          success: true,
          status: true,
          data: { userId: id.customerID, refId: id.ReferenceID },
        });
      } else {
        res.send({
          success: true,
          status: false,
          data: result,
        });
      }
    }
  }
});

router.post("/uploadImg", async (req, res) => {
  let arrayInsert = [];

  await upload(req, res, async (err) => {
    if (err) {
      return res.send({
        success: true,
        status: false,
        message: "ผิดพลาดในการบันทึกไฟล์",
      });
    } else {
      const files = req.files;
      files.forEach((v, i) => {
        const strSql = `UPDATE reference_doc set
                        date = '${moment().format("YYYY/MM/DD HH:mm:ss")}',
                        doc${i + 1} = '${v.originalname}',
                        appv_doc1 = 0,
                        appv_doc2 = 0,
                        appv_doc3 = 0,
                        appv_doc4 = 0
                        WHERE docno = '${req.headers.id.split(":")[1]}' 
                        AND customerID ='${req.headers.id.split(":")[0]}'`;
        arrayInsert.push(strSql);
      });
      const result = await db.executeTransaction(arrayInsert);
      if (result.status === "success") {
        const strSql = `SELECT name,lastname,email FROM customers WHERE customerID = '${
          req.headers.id.split(":")[0]
        }'`;
        const result = await db.executeSQL(strSql);

        res.send({
          success: true,
          status: true,
          data: {
            name: result[0].name,
            lastname: result[0].lastname,
            email: result[0].email,
          },
        });
      } else {
        res.send({ success: true, status: false });
      }
    }
  });
});

// router.post("/chkUser", async (req, res) => {
//   const { value } = req.body;
//   res.send({ success: true, data: true });
// });

module.exports = router;
